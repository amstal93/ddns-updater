FROM alpine:3
RUN apk add bash curl
COPY . /usr/local/src/ddns-updater
WORKDIR /usr/local/src/ddns-updater
CMD ["/bin/bash", "/usr/local/src/ddns-updater/update-ddns"]
